		<header> 
		<div class="topbar">
			<div class="topbar-inner">
				<div class="container">
				<a id="skip-navigation" href="#main" title="Skip Navigation">Skip Navigation</a>
				<a href="<?php bloginfo('home')?>" title="Home"><span id="home-link">Home</span><span id="home-button"></span></a>
				<div id="logo"><span>Creative Commons</span></div>
					<ul id="short-menu" class="nav">
						<li class="dropdown">
							<a href="#about" class="dropdown-toggle">Menu</a>
								<ul class="menu-dropdown">
									<li><a href="/about">About</a></li>
									<li><a href="/licenses/">Licenses</a></li>
									<li><a href="/publicdomain/">Public Domain</a></li>
									<li><a href="https://donate.creativecommons.org">Support CC</a></li>
									<li><a href="/culture">Projects</a></li>
									<li><a href="/weblog">News</a></li>
								</ul>
						</li>
					</ul>
					<ul id="wide-menu" class="nav">
				    <li class="dropdown">
				    	<a href="/about" class="dropdown-toggle">About</a>
				    	<ul class="menu-dropdown">
						    <li><a href="/about">About CC</a></li>
						    <li><a href="/about/history">History</a></li>
						    <li class="divider"></li>
						    <li><a href="/who-uses-cc">Who Uses CC?</a></li>
						    <li><a href="http://wiki.creativecommons.org/Case_Studies">Case Studies</a></li>
						    <li><a href="/videos/">Videos about CC</a></li>
						    <li class="divider"></li>
						    <li><strong><span>The Team</span></strong></li>
						    <li><a href="/board">Board of Directors</a></li>
						    <li><a href="/staff">Staff</a></li>
						    <li><a href="http://wiki.creativecommons.org/CC_Affiliate_Network">Affiliate Network</a></li>
						    <li><a href="/opportunities">Job Opportunities</a></li>
						    <li class="divider"></li>
						    <li><a href="http://wiki.creativecommons.org/FAQ">Frequently Asked Questions</a></li>
						    <li><a href="/contact">Contact Us</a></li>
						    
				      </ul>
				    </li>
				    <li class="dropdown">
				    	<a href="/licenses/" class="dropdown-toggle">Licenses</a>
				    	<ul class="menu-dropdown">
		    <li><a href="/licenses/">About the Licenses</a></li>
		    <li><a href="/choose/">Choose a License</a></li>
		    <li><a href="http://search.creativecommons.org">Find licensed content</a></li>
						    <li class="divider"></li>
		    <li><span>Licensors</span></li>
		    <li><a href="http://wiki.creativecommons.org/Considerations_for_licensors_and_licensees#Considerations_for_licensors">Things to know before licensing</a></li>
		    <li><a href="http://wiki.creativecommons.org/FAQ#Do_I_need_to_sign_something_or_register_to_obtain_a_Creative_Commons_license.3F">Do I need to register my work?</a></li>
		    <li><a href="http://wiki.creativecommons.org/Marking_your_work_with_a_CC_license">Marking my work</a></li>
		    <li><a href="/examples">License examples</a></li>
						    <li class="divider"></li>
		    <li><span>Licensees</span></li>
		    <li><a href="http://wiki.creativecommons.org/Best_practices_for_attribution">Best practices for attribution</a></li>
			<li><a href="http://wiki.creativecommons.org/FAQ#Who_gives_permission_to_use_material_offered_under_Creative_Commons_licenses.3F">Getting permission</a></li>
						    <li class="divider"></li>
						    <li><span>Developers</span></li>
						    <li><a href="http://wiki.creativecommons.org/Integrate">App Integration</a></li>
						    <li><a href="http://wiki.creativecommons.org/Developer_Challenges">Developer Challenges</a></li>
						    <li><a href="http://wiki.creativecommons.org/Translate">Translation</a></li>
				      </ul>
				    </li>
				    <li class="dropdown">
				    	<a href="/publicdomain/" class="dropdown-toggle">Public Domain</a>
				    	<ul class="menu-dropdown">
					<li><a href="/publicdomain/">Our Public Domain Tools</a></li>	
					<li class="divider"></li>
						    <li><a href="/about/cc0">About CC0 Public Domain Dedication</a></li>
						    <li><a href="/choose/zero/">Choose CC0 Public Domain Dedication </a></li>
						    <li class="divider"></li>
						    <li><a href="/about/pdm">About Public Domain Mark</a></li>
						    <li><a href="/choose/mark/">Use Public Domain Mark</a></li>
						 </ul>
				    </li>
				    
				    <li class="dropdown">
				    	<a href="https://creativecommons.net/" class="dropdown-toggle">Support CC</a>
				    	<ul class="menu-dropdown">
				    		<li><a href="https://creativecommons.net/">Donate to Creative Commons</a></li>
						    <li><a href="http://store.creativecommons.org/">Buy CC Merchandise</a></li>
				    		<li><a href="/supporters">Our Supporters</a></li>
						    <li><a href="https://creativecommons.net/testimonials/">Testimonials</a></li>
				    	</ul>
				    </li>
				    
				    <li class="dropdown">
				    	<a href="/culture" class="dropdown-toggle">Projects</a>
				    	<ul class="menu-dropdown">
						    <li><a href="/culture">Culture</a></li>
						    <li><a href="/education">Education</a></li>
						    <li><a href="/publicpolicy">Public Policy</a></li>
						    <li><a href="/science">Science</a></li>
						    <li class="divider"></li>
						    <li><a href="http://open4us.org/">OPEN</a></li>
						    <li><a href="/music-communities">Music Communities</a></li>
						    <li><a href="http://openpolicynetwork.org/">Open Policy Network</a></li>
                            <li><a href="/record-labels">Record Labels</a></li>
                            <li><a href="http://schoolofopen.org/">School of Open</a></li>
                            <li><a href="http://wiki.creativecommons.org/4.0">Versioning the license suite</a></li>
				      </ul>
				    </li>
				    <li><a href="/weblog">Blog</a></li>
				    <li class="dropdown">
				    <a href="/weblog" class="dropdown-toggle">News</a>
				    	<ul class="menu-dropdown">
						    <li><a href="http://planet.creativecommons.org/">Affiliate News</a></li>
						    <li class="divider"></li>
						    <li><a href="/interviews">Interviews</a></li>
						    <li><a href="http://wiki.creativecommons.org/Events">Events</a></li>
						    <li><a href="/newsletter">Newsletters</a></li>
						    <li class="divider"></li>
						    <li><a href="/about/press">Press Room</a></li>
						 </ul>
					</li>
					<li><a href="https://summit.creativecommons.org/">Global Summit 2015</a></li>

				</ul>
				</div>
			</div>
		</div>
<?php include 'banner.php'; ?>
		</header>

